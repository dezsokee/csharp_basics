﻿class Program
{
    //the username and password for the 4th exercise
    const string username = "dezsokee";
    const string password = "szabijelszo123";

    //print individual characters of a string in reverse order solved by the woodcutter method
    static void reverseString(string s) {
        char[] charArray = new char[s.Length];
        int j = 0;

        Console.Write("The reverse order: ");
        for(int i = s.Length - 1; i >= 0; i--) {
            charArray[j] = s[i];
            j++;
        }

        for(int i = 0; i < s.Length; i++) {
            Console.Write(charArray[i]);
        }

        Console.WriteLine();
    }

    //print individual characters of a string in reverse order with optimized method
    static string reverseString2(string s)
    {
        char[] charArray = s.ToCharArray();
        Array.Reverse(charArray);
        return new string(charArray);
    }

    //sum of n natural numbers
    static void sumOfNNaturalNumbers(int n) {
        int sum = 0;

        for(int i = 1; i <= n; i++) {
            sum += i;
        }

        Console.WriteLine("The sum of " + n + " natural numbers: " + sum);
    }

    //multiplication table from 1 to n
    static void multiplicationTable(int n) {
        Console.WriteLine("The multiplication table of " + n + ": ");
        for(int i = 1; i <= n; i++) {
            for(int j = 1; j <= n; j++) {
                Console.WriteLine(i + " * " + j + " = " + i * j);
            }
            Console.WriteLine();
        }
    }

    //basic login form
    //the inputs of the user never will be null
    static void loginForm() {
        string usernameInput;
        string passwordInput;

        int attemptsCount = 0;

        while(attemptsCount != 3) {
            Console.WriteLine("Enter your username: ");
            usernameInput = Console.ReadLine() ?? "";

            Console.WriteLine("Enter your password: ");
            passwordInput = Console.ReadLine() ?? "";

            if(usernameInput == username && passwordInput == password) {
                Console.WriteLine("You have successfully logged in!");
                break;
            } else {
                Console.WriteLine("Wrong username or password!");

                if(attemptsCount < 2) {
                    Console.WriteLine("You have " + (2 - attemptsCount) + " attempts left!");
                } else {
                    Console.WriteLine("You have no more attempts left!");
                }

                attemptsCount++;
            }
        }
    }

    //convert a decimal number to binary
    static void decimalToBinary(int n) {
        int binaryNumber = 0;
        int remainder;
        int i = 1;

        while(n != 0) {
            remainder = n % 2;
            n /= 2;
            binaryNumber += remainder * i;
            i *= 10;
        }

        Console.WriteLine("The binary number is: " + binaryNumber);
    }

    static void Main(string[] args)
    {
        //test with the first, basic method
        Console.WriteLine("Hello World!");
        reverseString("Hello World!");

        //test with the second, optimized method
        Console.WriteLine("geza kek az eg");
        Console.Write("The reverse order: ");
        Console.WriteLine(reverseString2("geza kek az eg"));

        //test the sum of n natural numbers
        int n;
        Console.WriteLine("Enter the number up to which you want the sum: ");
        n = Convert.ToInt32(Console.ReadLine());
        sumOfNNaturalNumbers(n);


        //test the multiplication table
        Console.WriteLine("Enter the number of the multiplication table: ");
        n = Convert.ToInt32(Console.ReadLine());
        multiplicationTable(n);

        //test the login form function
        Console.WriteLine("Login form: ");
        loginForm();

        //test the decimal to binary function
        Console.WriteLine("Enter the decimal number: ");
        n = Convert.ToInt32(Console.ReadLine());
        decimalToBinary(n);


    }

    

}
